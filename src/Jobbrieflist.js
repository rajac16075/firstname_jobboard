import React from 'react';

class Jobbrieflist extends React.Component{
   
    render() {
        return(
            <div>
              <h4>{this.props.name}</h4>
              <h5>{this.props.place}</h5>
              <img alt="img" src="https://picsum.photos/id/180/200/100" className="job-post-image" />
              <p>
                {this.props.desc}
              </p>
              <div className="salary">{this.props.salary}</div>
              <button style={{backgroundColor: 'blue', color: 'white'}}>Apply</button>
              <button style={{backgroundColor: 'red', color: 'white'}}>Not interested</button>
              <hr />
            </div>
        );
    }
};
export default Jobbrieflist;

