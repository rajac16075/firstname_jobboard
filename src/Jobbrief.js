import React from 'react';

class Jobbrief extends React.Component{
    render() {
        return(
            <div>
              <h4>Full-stack developer</h4>
              <h5>Delhi</h5>
              <img alt="img" src="https://picsum.photos/id/180/200/100" className="job-post-image" />
              <p>
          
              </p>
              <div className="salary">Salary: 30K - 40K</div>
              <button style={{backgroundColor: 'blue', color: 'white'}}>Apply</button>
              <button style={{backgroundColor: 'red', color: 'white'}}>Not interested</button>
              <hr />
            </div>
        );
    }
};
export default Jobbrief

