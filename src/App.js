import React from 'react';
import Jobbrieflist from './Jobbrieflist'
class App extends React.Component{
    
    render(){
        return(<div> 
            <Jobbrieflist
            name= "FullStackDeveloper"
            place="Theni"
            desc='Company looking for some crazy people, we are looking for challenge accepter who move with "I can do" attitude.
            Quick decision makers, living with the passion of work, never feeling or saying "I am tired"'
            salary="Salary: 30K - 40K"
            />
            <Jobbrieflist
            name= "Front-end developer"
            place="Pune"
            desc='Company looking for some crazy people, we are looking for challenge accepter who move with "I can do" attitude.
            Quick decision makers, living with the passion of work, never feeling or saying "I am tired"'
            salary="Salary: 15K - 30K"
            />
            <Jobbrieflist
            name= "Back-end developer"
            place="Mumbai"
            desc='Company looking for some crazy people, we are looking for challenge accepter who move with "I can do" attitude.
            Quick decision makers, living with the passion of work, never feeling or saying "I am tired"'
            salary="Salary: 20K - 30K"
            />
            <Jobbrieflist
            name= "HR"
            place="Chennai"
            desc='Company looking for some crazy people, we are looking for challenge accepter who move with "I can do" attitude.
            Quick decision makers, living with the passion of work, never feeling or saying "I am tired"'
            salary="Salary: 25K - 30K"
            />
        </div>);

    }
   
}
export default App;